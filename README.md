# Dotfiles Manager

A simple dotfiles manager using a bare git repo. Most of the work is done in the setup script (`dotfiles-init`). Everything after that is handled by the `dotfiles` command which is a simple alias for git.

## Setup

```
Usage: dotfiles-init [OPTION]...
Setup dotfiles, either cloning from or creating a git repo.

-d directory      The location to clone the dotfiles manager into.
                  Default is "~/.dotfiles".
-r repo           The location for the dotfiles repo. Default is
                  "~/.dotfiles/repo".
-o origin         The optional origin for the dotfiles repo. If empty,
                  a new repo will be created and pushed to it. If not,
                  the repo will be cloned.
-h                Print this help message.
```

### Examples

#### Setup with a new repository

```shell
sh <(curl https://gitlab.com/sommd/dotfiles-manager/raw/master/dotfiles-init)
```

#### Setup with an existing repository

```shell
sh <(curl https://gitlab.com/sommd/dotfiles-manager/raw/master/dotfiles-init) -o <existing repository>
```

If the repo is not empty, it will be cloned. If the repo is empty, a new repository will be created and pushed.

## Usage

```shell
dotfiles ...
```

`dotfiles` is an alias for git with the dotfiles repo, so all commands are the same as git. By default, only tracked files are shown and the dotfiles manager directory and your dotfiles repository are ignored.

### Examples

#### Add some files and push

```shell
dotfiles add .bashrc .bash_profile
dotfiles commit -m "Add bash config files"
dotfiles push
```

#### Update dotfiles

```shell
dotfiles pull
```
